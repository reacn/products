﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Products.Data;
using Products.Models;

namespace Products.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ProductsDBContext _context;

        public OrdersController(ProductsDBContext context)
        {
            _context = context;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrders()
        {
            var handler = new JwtSecurityTokenHandler();
            string authHeader = Request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", "");
            var token = handler.ReadToken(authHeader) as JwtSecurityToken;

            return await _context.Orders.Include(o => o.OrderedProducts).Include(s => s.Status).Where(c => c.CustomerId == token.Subject).ToListAsync();
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrder(Guid id)
        {
            var handler = new JwtSecurityTokenHandler();
            string authHeader = Request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", "");
            var token = handler.ReadToken(authHeader) as JwtSecurityToken;

            var order = await _context.Orders.Include(o => o.OrderedProducts).Include(s => s.Status).Where(c => c.CustomerId == token.Subject).FirstOrDefaultAsync(i => i.Id == id);

            if (order == null)
            {
                return NotFound();
            }

            return order;
        }        

        // POST: api/Orders
        [HttpPost]
        public async Task<ActionResult<string>> PostOrder(Order order)
        {
            var handler = new JwtSecurityTokenHandler();
            string authHeader = Request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", "");
            var token = handler.ReadToken(authHeader) as JwtSecurityToken;
            order.CustomerEmail = token.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
            order.CustomerId = token.Subject;
            order.CreatedAt = DateTimeOffset.Now;
            order.Lang = "Pl";
            order.Status.State = "Initialize";

            var pordered = order.OrderedProducts;

            double sum = 0;

            foreach (var p in pordered)
            {
                var pp = await _context.Products.FindAsync(p.ProductId);

                sum += p.Count * pp.Price;

            }
            sum = Math.Round(sum, 3);

            order.PriceSum = sum;


            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            string ShopId = Environment.GetEnvironmentVariable("ShopId");
            string beforehash = Environment.GetEnvironmentVariable("ShopPin") + ShopId + $"{sum}PLNTestreacn.pl{order.CustomerEmail}";

            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(beforehash));

            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }

            string url = $"https://ssl.dotpay.pl/test_payment/?id={ShopId}&amount={sum}&currency=PLN&description=Test&url=reacn.pl&email={order.CustomerEmail}&chk={hash.ToString()}";
            return Ok(new string(url));

        } 
    }
}