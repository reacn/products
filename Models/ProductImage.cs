﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.Models
{
    public class ProductImage
    {
        [Key]
        public Guid Id { get; set; }

        [Url]
        [Required]
        public string ImageUrl { get; set; }

        public Guid? ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
