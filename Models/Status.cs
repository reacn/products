﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.Models
{
    public class Status
    {
        [Key]
        public Guid Id { get; set; }

        public Guid? OrderId { get; set; }

        public virtual Order Order { get; set; }

        public string State { get; set; }
    }
}
