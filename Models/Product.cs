﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Products.Models
{
    public class Product
    {
        public Product()
        {
            ProductImages = new List<ProductImage>();
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set;}

        [Required]
        public string Description { get; set; }

        [Required]
        public double Price { get; set; }

        public DateTimeOffset AddDate { get; set; }

        [Required]
        public ICollection<ProductImage> ProductImages { get; set; }
    }
}
