﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.Models
{
    public class Order
    {
        public Order()
        {
            OrderedProducts = new List<OrderedProduct>();
            Status = new Status();
        }

        [Key]
        public Guid Id { get; set; }

        public string CustomerId { get; set; }

        [EmailAddress]
        public string CustomerEmail { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public double PriceSum { get; set; }

        public string Lang { get; set; }

        [Required]
        public virtual ICollection<OrderedProduct> OrderedProducts { get; set; }

        public virtual Status Status { get; set; }
    }
}
