﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Products.Models;

namespace Products.Data

{
    public class ProductsDBContext : DbContext
    {

        public ProductsDBContext(DbContextOptions<ProductsDBContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderedProduct> OrderedProducts { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Status> Status { get; set; }
    }
}
